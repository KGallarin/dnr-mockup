# Project Title

Mock up exam from Dog and Rooster Inc. A simple html page created with bootstrap and sass-gulp to enable pre-compiling with CSS. Also ready for prototyping with gulp-connect server.

## Getting Started

Package.json is ready to serve the necessary packages that includes gulp-connect and gulp-watch.

### Prerequisites

You need to have NodeJS installed in your machine for running the sass watcher and the server.

### Installing

Please run

```
$ npm install

```

Then run it to your browser via localhost:8080. 


Please refresh the page before hitting the menu (logo) for some testing.


## Built With

* [Bootstrap](https://getbootstrap.com/) - The CSS framework used
* [SASS](https://sass-lang.com/) - CSS Precompiler
* [GULP](https://gulpjs.com/) - JS used for setup


## Author

* **Kevin M. Gallarin** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* A little refreshed with Bootstrap
* Good Mockup


