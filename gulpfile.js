'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

var server = require("gulp-connect");

gulp.task('default',['sass','server','sass:watch']);


gulp.task('sass',function () {
	return gulp.src('./styles/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./styles/'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./styles/scss/**/*.scss', ['sass']);
});

gulp.task('server',function () {
	server.server();
});

